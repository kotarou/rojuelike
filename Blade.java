import java.util.Random;
import java.awt.Color;
/**
 * Blade is the element characterised by
 * high damage, medium range, low duration
 */
public class Blade extends Element
{
    private Random rand;

    public Blade()
    {
        this.name = "Blade";

        this.chargeMin = 1;
        this.chargeMax = 4;
        this.rand = new Random();
        this.charges = rand.nextInt((this.chargeMax - this.chargeMin) + 1) + this.chargeMin;

        this.prefixes   = new String[]{"Barbed", "Bladed", "Black"};
        this.roots      = new String[]{"Blade", "Barb", "Broadsword", "Battleaxe", "Beard"};
        this.suffixes   = new String[]{"Bone", "Bifurcation", "Brutality", "Backstabbing"};

        this.range      = rand.nextInt((5 - 1) + 1) + 1; // 0 is personal, n = dist
        this.m_range    = 1.2;
        this.targetType = rand.nextInt((2 - 1) + 1) + 1; // 0 = buff, 1 = line, 2 = PBAoE
        this.damageType = rand.nextInt((1 - 0) + 1) + 0; // 0 = normal, 1 = penetrating
        this.statusAff  = 0; // 0 = none, 1 = haste, 2 = slow, 3 = poison
        this.duration   = rand.nextInt((3 - 1) + 1) + 1; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)
        this.m_duration = 1;
        this.damage     = rand.nextInt(60)+10;
        this.m_damage   = 3;
        this.self_damage = 0;
        this.color = Color.DARK_GRAY;
    }

    // Prefix
    public double prefix_mod(){
        return 1.5;
    }

    // Effect
    public double effect_mod(){
        return 1.5;
    }

    // Suffix
    public double suffix_mod(){
        return 1.5;
    }

}







import ecs100.*;
import java.awt.Color;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.awt.Graphics2D;
import javax.swing.JLabel;
import java.util.Random;

/**
 * Write a description of class board here.
 *
 * @author kotarou
 */
public class Board
{
    // instance variables - replace the example below with your own
    public int displayWidth;
    public int displayHeight;

    public int mapWidth;
    public int mapHeight;

    //public Entity[][] map;
    //public Entity[][] backMap;

    public int charWidth;

    public int topStatusHeight  = 2;
    public int bottomInputHeight= 6;

    public ArrayDeque<String> status;

    public LinkedList<Level> levels;
    public int levelIndex = 0;

    public Level level;
    public int offsetX = 0;
    public int offsetY = 0;

    public Game game;

    /**
     * Constructor for objects of class board
     */
    public Board(int w, int h, int charWidth, LinkedList<Level> levels, Game game)
    {
        // Define the size of the screen area that can be rendered to



        this.displayWidth  = w - 1;
        int tempHeight = h;
        this.displayHeight = tempHeight - (topStatusHeight + bottomInputHeight);

        this.charWidth = charWidth;

        this.levels = levels;
        this.level = levels.getFirst();

        Player player = new Player(this);
        this.game = game;
        this.game.player = player;

        System.out.println("Player created, game refferred.");

        // Set the board up
        this.setUpBoard();

        System.out.println("Board set up.");

        // Status messages.
        this.status = new ArrayDeque<>();
        this.statusAdd("Welcome to rojuelike! --more--");
        this.statusAdd("You are the hopeful apprentice of the mighty wizard robert (hopefully) --more--");
        this.statusAdd("You have entered this dungeon with the hope of proving your magic --more--");
        this.statusAdd("Unfortunately you are running out of energy to recast your spells --more--");
        this.statusAdd("Find elemental nodes on the dungeon floor and combine them to... --more--");
        this.statusAdd("make new and exiciting spells to --more--");

    }

    private void setUpBoard(){

        this.offsetX = (this.displayWidth / 2) - (game.player.posX); //(this.level.width / 2);
        this.offsetY = (this.displayHeight / 2) - (game.player.posY); //(this.level.width / 2);
        System.out.println("Offsets set");

        // evilllllll
        if(!this.level.stairDownGenerated){
            this.addEntity(new Kobold(this));
            this.addEntity(new Demon(this));
            this.addEntity(new Dragon(this));

            System.out.println("Hard coded creatures added");

            Random rand = new Random();
            int placed = 0;
            while(placed < 50){
                int num = rand.nextInt(rand.nextInt((99) + 1) + 1);
                boolean t = false;
                if( num < 10 )
                    t = this.dropEntity(new Kobold(this));
                else if( num < 40 )
                    t = this.dropEntity(new Dog(this));
                else if ( num < 50 )
                    t = this.dropEntity(new Component(this, this.game.player, new Blade(), false));
                else if ( num < 60 )
                    t = this.dropEntity(new Component(this, this.game.player, new Time(), false));
                else if ( num < 70 )
                    t = this.dropEntity(new Component(this, this.game.player, new Radiation(), false));
                else if ( num < 80 )
                    t = this.dropEntity(new Component(this, this.game.player, new Stitch(), false));
                else if ( num < 90 )
                    t = this.dropEntity(new Component(this, this.game.player, new Greed(), false));
                else
                    t = this.dropEntity(new Demon(this));
                if(t)
                    placed++;
            }
        }


        // this.levelIndex < this.levels.size() - 1 &&  no longer relevant because we dynamically generate new levels
        if(!this.level.stairDownGenerated){
            this.level.stairDownGenerated = true;
            this.repeatDropEntity(new StairDown());
            this.level.backMap[this.level.objects.get(this.level.objects.size()-1).posX][this.level.objects.get(this.level.objects.size()-1).posY] = this.level.objects.get(this.level.objects.size()-1);
        }

        System.out.println("Stairs added");

        this.addEntity(this.game.player);

    }


    public void addEntity(Entity p){
        //TODO: make sure the space is legal
        this.level.map[p.posX][p.posY] = p;
        //System.out.println(this.level.objects);
        //System.out.println(p);
        this.level.objects.add(p);
    }

    public void move(int oldX, int oldY, int newX, int newY){

        // if(this.board.level.backMap[this.posX][this.posY].name.equals("stairDown")){
        //     this.board.statusAddHead("Descending staircase!");
        // }
        // if(this.board.level.backMap[this.posX][this.posY].name.equals("stairUp")){
        //     this.board.statusAddHead("Ascending staircase!");
        // }


        // Move entity to a new location
        Entity temp = this.level.map[oldX][oldY];
        this.level.map[oldX][oldY] = this.level.backMap[oldX][oldY];
        //this.level.map[newX][newY] = temp;

        if(temp.name.equals("player") && this.level.map[newX][newY].name.equals("stairUp")){
            //this.level.map[newX][newY] = temp;
            this.statusAddHead("Ascending the stairs!");

            this.levelIndex--;
            //this.level.objects.remove(this.game.player);
            this.level = levels.get(this.levelIndex);
            this.setUpBoard();

            // if(this.levelIndex > 0){
            //     StairUp st = new StairDown();
            //     st.posX = newX;
            //     st.posY = newY;
            //     this.addEntity(st);
            //     this.level.backMap[newX][newY] = st;
            // }
            //this.game.player.posX = newX; this.game.player.posY = newY;
            this.level.map[newX][newY] = temp;

        } else if( temp.name.equals("player") && this.level.map[newX][newY].name.equals("stairDown")){

            while(true){
                Level l = new Level();
                if(l.map[newX][newY].name.equals("floor")){
                    this.levels.add(l);
                    break;
                }
            }


            //this.level.map[newX][newY] = temp;
            this.statusAddHead("Descending the stairs!");
            //this.level.objects.remove(this.game.player);
            this.levelIndex++;

            this.level = levels.get(this.levelIndex);
            this.setUpBoard();

            if(this.levelIndex > 0){
                StairUp st = new StairUp();
                st.posX = newX;
                st.posY = newY;
                this.addEntity(st);
                // TODO: make sure we are a valid location
                if(this.level.backMap[newX][newY].name.equals("wall")){
                    // We are in a wall! Shit.
                    // Modify the map
                    level.makeLegal(newX, newY);
                }

                this.level.backMap[newX][newY] = st;
            }
            this.level.map[newX][newY] = temp;
            //this.game.player.posX = newX; this.game.player.posY = newY;
        } else {
            this.level.map[newX][newY] = temp;
        }


        //System.out.println(temp + " moving to " + newX + " " + newY + " from " + oldX + " " + oldY);
        //this.map[oldX][oldY] = new Water(); //this.backMap[oldX][oldY];
    }

    public void statusAddHead(String msg){
        this.status.addFirst(msg);
    }

    public void statusAdd(String msg){
        this.status.add(msg);
    }

    public void clearStatus(){
        this.status.clear();
        this.status.add(" ");
    }


    public String readStatus(){
        String out = this.status.peek();
        // if(this.status.size() > 1)
        //     out = out + " --more--";
        return out;
    }

    public void updateStatus(){
        this.status.poll();
        if(this.status.isEmpty())
            this.status.add("");
    }

    public boolean repeatDropEntity(Entity ent){
        while(true){
            if(dropEntity(ent))
                break;
        }
        return true;
    }

    public boolean dropEntity(Entity ent){
        Random rand = new Random();
        int x = rand.nextInt(this.level.width);
        int y = rand.nextInt(this.level.height);
        if(this.level.map[x][y].name.equals("floor")){
            ent.posX = x;
            ent.posY = y;
            this.addEntity(ent);
            return true;
        }
        return false;
    }

    public void renderString(Graphics2D gPanel, String inString, int x, int y){
        String in = "<html>"+ inString + "</html>";
        JLabel jl = new JLabel(in);
        //jl.setDoubleBuffered(true);
        jl.setBounds(0, 0, Math.max(jl.getPreferredSize().width, in.length() * 10), (int)(jl.getPreferredSize().height * 1.1));
        jl.setFont(game.fontUI);

        gPanel.translate(x,y);
        jl.paint(gPanel);
        gPanel.translate(-x, -y);
    }

    public int render(Graphics2D gPanel){
        //TODO: remove hard coded string locations

        // Render the level
        //UI.setFontSize(12);
        // Loop over each "pixel" and draw them if required
        UI.setColor(Color.WHITE);

        UI.getGraphics().setFont(game.fontBoard);
        for(int i = 0; i < this.displayWidth; i++){
            for(int j = 0; j < this.displayHeight; j++){

                // Convert screen space to map-space
                int x = Math.min(Math.max(-1, i - offsetX), this.level.width);
                int y = Math.min(Math.max(-1, j - offsetY), this.level.height);

                if(x == -1 || y == -1 || x == this.level.width || y == this.level.height || this.level.map[x][y] == null){
                    //UI.setColor(Color.WHITE);
                    UI.drawString(" ", i*charWidth+5, (j+1+topStatusHeight)*charWidth);
                }
                else{
                    // this.renderString(gPanel, "<span style='color: "+this.map[x][y].htmlColor+";'>"+this.map[x][y].character+"</span>",
                    //                   i*charWidth+5, (j+1+topStatusHeight)*charWidth);
                    if(this.game.player.visible[x][y]){
                        if ((this.level.volatileMap[x][y] != null) && (!(this.level.map[x][y] instanceof Creature))){

                            //System.out.println((this.level.volatileMap[x][y]).getClass().getName());
                            //System.out.println(this.level.volatileMap[x][y].color);
                            //System.out.println(this.level.volatileMap[x][y].character);
                            //System.out.println(this.level.volatileMap[x][y].htmlColor);

                            UI.setColor(this.level.volatileMap[x][y].color);
                            UI.drawString(this.level.volatileMap[x][y].character, i*charWidth+5,
                                           (j+1+topStatusHeight)*(charWidth+1));
                        }
                        else if(!this.level.map[x][y].invisible){
                            UI.setColor(this.level.map[x][y].color);
                            UI.drawString(this.level.map[x][y].character, i*charWidth+5,
                                           (j+1+topStatusHeight)*(charWidth+1));
                        } else {
                            UI.setColor(this.level.backMap[x][y].color);
                            UI.drawString(this.level.backMap[x][y].character, i*charWidth+5,
                                           (j+1+topStatusHeight)*(charWidth+1));
                        }
                    } else {
                        UI.drawString(" ", i*charWidth+5,
                                       (j+1+topStatusHeight)*(charWidth+1));
                    }
                }
            }
        }

        // cleat the volatile map
        for(int i = 0; i < this.level.width; i++){
            for(int j = 0; j < this.level.height; j++){
                this.level.volatileMap[i][j] = null;
            }
        }


        // Render the status menu
        this.renderString(gPanel, "<span style='color: #ffffff;'>"+this.readStatus()+"</span>", 3, 7);

        // The player can be looking at menu 0: Inventory or menu 1: Spell List
        boolean inv = this.game.player.menu != 0;
        boolean spl = !inv;

        UI.setColor(Color.BLACK);
        UI.fillRect(0, 530, 800, 300);

        // Backing for the Spell List
        UI.fillRect(677, 438, 230, (spl ? 297 : 63)) ;

        // Backing for the inventory
        if(inv)
            UI.fillRect(554, 433, 125, (inv ? 297 : 63));

        // Name and details
        this.renderString(gPanel, "<span style='color: #ffffff;'>Generic Wizard \t level: irrelevant</span>", 3, 530);



        // Top border
        this.renderString(gPanel, "<span style='color: #ffffff;'>"+new String(new char[(spl ? 68 : 80 )]).replace("\0", "-")+"</span>", 3, 543);

        // Inventory list top
        this.renderString(gPanel, "<span style='color: #ffffff;'>"+new String(new char[12]).replace("\0", "-")+"</span>", 562, (inv ? 443 : 513) );
        // Inventory title
        this.renderString(gPanel, "<span style='color: green'>I</span><span style='color: white;'>nventory</span>", 567, (inv ? 457 : 527 ) );
        // Inventory list side
        for(int i = 0; i < (inv ? 10 : 3 ); i++)
            this.renderString(gPanel, "<span style='color: #ffffff;'>|</span>", 557, 538-(i*10));
        // Inventory
        if (inv)
            this.renderString(gPanel, this.game.player.buildInventoryString(), 575, 485);

        // Spell list top
        this.renderString(gPanel, "<span style='color: #ffffff;'>"+new String(new char[15]).replace("\0", "-")+"</span>", 680, (spl ? 443 : 513) );
        // SpellList title
        this.renderString(gPanel, "<span style='color: white;'>S</span><span style='color: green;'>p</span><span style='color: white;'>ell List</span>", 689, (spl ? 457 : 527 ) );
        // Spell list side
        for(int i = 0; i < 10; i++)
            this.renderString(gPanel, "<span style='color: #ffffff;'>|</span>", 677, 538-(i*10));
        // Spells
        if (spl)
            this.renderString(gPanel, this.game.player.buildSpellListString(), 695, 490);

        this.renderString(gPanel, this.game.player.buildStatusString(), 6, 558);
        this.renderString(gPanel, this.game.player.buildCurrentSpellString(), 6, 578);






        return 0;
    }

    public boolean allowsMove(int x, int y){
        //System.out.println(x + " " + y + " occupised by: " + this.level.map[x][y] + " and is pathable: " + this.level.map[x][y].pathable);
        return this.level.map[x][y].pathable;
    }

    public Entity allowsAttack(int x, int y){
        if (this.level.map[x][y].destructable == true){
            return this.level.map[x][y];
        }
        else{
            return null;
        }
    }
}

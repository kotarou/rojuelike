import java.awt.Color;
import java.util.Random;

public class Component extends Item
{
    // instance variables - replace the example below with your own
    public boolean inworld;
    public boolean onPlayer;
    private Player player;
    public Element element;

    public Component(Board board, Player player, Element element, boolean onPlayer){
        super();

        Random rand = new Random();
        this.board = board;
        this.player = player;

        this.element = element;
        this.character = "?";
        this.color = Color.BLUE;
        this.htmlColor = "00ff00";

        this.name = "Component";

        this.invisible = onPlayer;
        this.intelligent = true;
        this.pathable = true;

        this.onPlayer = onPlayer;

        if(this.onPlayer){
            this.posX = player.posX;
            this.posY = player.posY;
        } else {
            this.posX = 65+rand.nextInt((10 - 5) + 1) + 5;
            this.posY = 70+rand.nextInt((4 - (-5)) + 1) + (-5);
        }
    }

    // This adds it to the world
    public void drop(){
        this.invisible  = false;
        this.onPlayer   = false;
    }

    public void act(){
        if(this.onPlayer){
            this.posX = player.posX;
            this.posY = player.posY;
        }
        if(this.posX == player.posX && this.posY == player.posY && !this.onPlayer){
            this.onPlayer = true;
            player.pickup_component(this.element);

        }
    }

    public void pickup(){
        if (this.player.inventory.size() > 2){
            // no item pickup inventory full
        }

        else{
            this.inworld = false;
            this.player.inventory.add(element);
        }
    }
}

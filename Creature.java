import java.awt.Color;
public abstract class Creature extends Entity
{

    public double maxHp;
    public double hp;
    public int damage;


    public boolean alive(){
        return this.hp > 0;
    }

    @Override
    public boolean applyDamage(int damage){
        this.hp -= damage;
        if(this.hp <= 0){
            this.die();
            this.intelligent = false;
        }
        return true;
    }


    public void die(){
        if(this.board.level.backMap[this.posX][this.posY].name.equals("floor"))
            this.board.level.backMap[this.posX][this.posY] = this;
        this.color = Color.RED;
        this.htmlColor = "#ff0000";

        this.movable        = false;
        this.intelligent    = false;
        this.destructable   = false;
        this.pathable       = true;
        this.board.statusAddHead("The " + this.name + " breathes its last breath!");
    }

    public void move(int direction){
        if(!this.movable)
            return;
        // // TODO: support greater than 1 distances
        int distance = 1;

        // NORTH
        if(direction == 0 && this.board.allowsMove(this.posX, this.posY - 1)){
            this.board.move(this.posX, this.posY, this.posX, this.posY - 1);
            this.posY -= distance;
        }
        // EAST
        else if(direction == 1 && this.board.allowsMove(this.posX + 1, this.posY)){
            this.board.move(this.posX, this.posY, this.posX + 1, this.posY);
            this.posX += distance;
        }
        // SOUTH
        else if(direction == 2 && this.board.allowsMove(this.posX, this.posY + 1)){
            this.board.move(this.posX, this.posY, this.posX, this.posY + 1);
            this.posY += distance;
        }
        // WEST
        else if(direction == 3 && this.board.allowsMove(this.posX - 1, this.posY)){
            this.board.move(this.posX, this.posY, this.posX - 1, this.posY);
            this.posX -= distance;
        }


        // if(!this.name.equals("player"))
        //     return;


    }

}

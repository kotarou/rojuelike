import java.awt.Color;

/**
 * Write a description of class Kobold here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Demon extends Creature
{
    // instance variables - replace the example below with your own
    /**
     * Constructor for objects of class Kobold
     */
    public Creature enemy;
    private int range = 1;

    public Demon(Board board)
    {
        this.character = "&";
        this.color = Color.RED;
        this.htmlColor = "#ff0000";

        this.name = "Demon";

        this.hp = 300.0;
        this.maxHp = 300.0;
        this.damage = 30;

        this.posX = 76;
        this.posY = 76;

        this.board = board;

        this.pathable = false;
        this.movable = true;
        this.intelligent = true;
        this.pushable = false;
        this.destructable = true;

        this.enemy = board.game.player;
    }


    public void act(){

        // Are we with range of the target
        if(Math.pow(enemy.posX - this.posX, 2) + Math.pow(enemy.posY - this.posY, 2) <= Math.pow(this.range, 2)){
            if(this.enemy.alive())
                this.attack(this.enemy);
        }

        // Otherwise, lets move towards it
        this.moveTowards(this.enemy);
    }

    private void moveTowards(Entity target){
        int x = target.posX;
        int y = target.posY;

        int direction = 0;

        if(Math.abs(x - this.posX) > Math.abs(y - this.posY)){
            // Move along x first
            direction = (x > this.posX ? 1 : 3);

        } else{
            // Move along y first
            direction = (y > this.posY ? 2 : 0);
        }

        this.move(direction);
    }

    public void die(){
        this.character = "%";
        this.color = Color.RED;
        this.htmlColor = "#ff0000";

        this.movable        = false;
        this.intelligent    = false;
        this.destructable   = false;
        this.pathable       = true;
    }

    private void attack(Creature target){
        this.board.statusAddHead("The demon gouges you for  " + this.damage + " damage!");
        target.applyDamage(this.damage);
    }

}

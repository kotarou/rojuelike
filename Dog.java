import java.awt.Color;

/**
 * Write a description of class Kobold here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Dog extends Creature
{
    // instance variables - replace the example below with your own
    /**
     * Constructor for objects of class Kobold
     */
    public Creature enemy;
    private int range = 1;

    public Dog(Board board)
    {
        this.character = "d";
        this.color = Color.CYAN;
        this.htmlColor = "#008888";

        this.name = "Dog";

        this.hp = 5.0;
        this.maxHp = 5.0;
        this.damage = 3;

        this.posX = 3;
        this.posY = 3;

        this.board = board;

        this.pathable = false;
        this.movable = true;
        this.intelligent = true;
        this.pushable = false;
        this.destructable = true;

        this.enemy = board.game.player;
    }


    public void act(){

        // Are we with range of the target
        if(Math.pow(enemy.posX - this.posX, 2) + Math.pow(enemy.posY - this.posY, 2) <= Math.pow(this.range, 2)){
            if(this.enemy.alive())
                this.attack(this.enemy);
        }

        // Otherwise, lets move towards it
        this.moveTowards(this.enemy);
    }

    private void moveTowards(Entity target){
        int x = target.posX;
        int y = target.posY;

        int direction = 0;

        if(Math.abs(x - this.posX) > Math.abs(y - this.posY)){
            // Move along x first
            direction = (x > this.posX ? 1 : 3);

        } else{
            // Move along y first
            direction = (y > this.posY ? 2 : 0);
        }

        super.move(direction);
    }

    public void die(){
        super.die();
        this.character = "*";
    }

    private void attack(Creature target){
        this.board.statusAddHead("The dog nips you for  " + this.damage + " damage!");
        target.applyDamage(this.damage);
    }

}

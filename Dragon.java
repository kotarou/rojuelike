import java.awt.Color;
import java.util.Random;

public class Dragon extends Creature
{

    public Creature enemy;
    private int meleeRange = 1;
    private int fireRange = 3;
    private Random rand;
    private int meleeDamage = 40;;
    private int fireDamage = 30;

    public Dragon(Board board)
    {
        this.character = "D";
        this.color = Color.RED.darker();
        this.htmlColor = "#ff0000";

        this.name = "DRAGON";

        this.hp = 500.0;
        this.maxHp = 500.0;

        this.posX = 81;
        this.posY = 81;

        this.board = board;

        this.pathable = false;
        this.movable = true;
        this.intelligent = true;
        this.pushable = false;
        this.destructable = true;

        this.enemy = board.game.player;


        this.rand = new Random();
    }


    public void act(){

        // Are we with range of the target if so melee it
        if(Math.pow(enemy.posX - this.posX, 2) + Math.pow(enemy.posY - this.posY, 2) <= Math.pow(this.meleeRange, 2)){
            if(this.enemy.alive())
                this.meleeAttack(this.enemy);
        }


        // Otherwise randomly shoot or move
        else if(Math.pow(enemy.posX - this.posX, 2) + Math.pow(enemy.posY - this.posY, 2) <= Math.pow(this.fireRange, 2)){

            if(rand.nextInt(2) == 1){
                if(this.enemy.alive()){
                    this.fireAttack(this.enemy);
                }
            }
            else{
                // Otherwise, lets move towards it
                System.out.println("attackmove");
                this.moveTowards(this.enemy);
            }
        }

        // Otherwise, lets move towards it
        else{
        this.moveTowards(this.enemy);
        System.out.println("move");
        }
    }



    private void moveTowards(Entity target){
        int x = target.posX;
        int y = target.posY;

        int direction = 0;

        if(Math.abs(x - this.posX) > Math.abs(y - this.posY)){
            // Move along x first
            direction = (x > this.posX ? 1 : 3);

        } else{
            // Move along y first
            direction = (y > this.posY ? 2 : 0);
        }

        this.move(direction);
    }

    public void die(){
        this.character = "$";
        this.color = Color.RED;
        this.htmlColor = "#ff0000";

        this.movable        = false;
        this.intelligent    = false;
        this.destructable   = false;
        this.pathable       = true;
    }

    private void meleeAttack(Creature target){
        this.board.statusAddHead("The dragon claws you for  " + this.damage + " damage!");
        target.applyDamage(this.meleeDamage);
    }

    private void fireAttack(Creature target){
        this.board.statusAddHead("The dragon breathes fire!");
        target.applyDamage(this.fireDamage);
    }

}

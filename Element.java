import java.util.Random;
import java.awt.Color;

abstract class Element
{
    public int range; // 0 is personal, n = dist
    public double m_range;
    public int targetType; // 0 = buff, 1 = line, 2 = PBAoE
    public int damageType; // 0 = normal, 1 = penetrating
    public int statusAff; // 0 = none, 1 = haste, 2 = slow, 3 = poison
    public int duration; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)
    public double m_duration;
    public int damage;
    public double m_damage;
    public int self_damage;
    private Random rand;
    public String[] prefixes;
    public String[] roots;
    public String[] suffixes;

    public Color color;
    public String name;

    public String descriptiveName(int position){
        this.rand = new Random();
        if(position == 0){
            int sel = this.rand.nextInt((this.prefixes.length - 1 - 0) + 1) + 0;
            return this.prefixes[sel];
        }
        if(position == 1){
            int sel = this.rand.nextInt((this.roots.length - 1 - 0) + 1) + 0;
            return this.roots[sel];
        }
        if(position == 2){
            int sel = this.rand.nextInt((this.suffixes.length - 1 - 0) + 1) + 0;
            return this.suffixes[sel];
        }
        return "";
    }

    public int chargeMin;
    public int chargeMax;

    public int charges;

}

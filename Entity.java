import java.awt.Color;
/**
 * Entity is the interface for objects that appear within the game map
 *
 */
public abstract class Entity
{
    public String character;
    public Color color;
    public String htmlColor;

    public int posX;
    public int posY;

    public String name = "";

    public Board board;

    // Can this move of its own accord?
    public boolean movable;//     = false;
    // Can this move by being walked into by the player or a NPC?
    public boolean pushable     = true;
    // Can this be destroyed?
    public boolean destructable;// = false;
    // Can a player or NPC walk over this?
    public boolean pathable;//     = true;
    // Can this make actions at all?
    public boolean intelligent;//  = false;
    // Can this be seen or interacted with?
    public boolean invisible = false;

    public abstract void act();

    public boolean applyDamage(int damage){return false;};

}

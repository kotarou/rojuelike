import java.awt.Color;

public class Floor extends Entity
{

    public Floor()
    {
        this.character = ".";
        this.color = Color.WHITE;
        this.htmlColor = "#ffffff";
        this.name = "floor";

        this.pathable       = true;
        this.movable        = false;
        this.intelligent    = false;
        this.pushable       = false;
        this.destructable   = false;
    }

    // bah
    public void act(){}
}

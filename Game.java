/**
 * Main class game
 * This is the main game class, where we set up the game state
 *
 * @author kotarou
 */

import ecs100.*;
import java.util.*;
import java.io.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import java.util.Random;

//import java.lang.reflect.*;

public class Game{
    // Game variables
    final int TARGET_FPS    = 120;
    final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;
    final int WINDOW_WIDTH  = 800;
    final int WINDOW_HEIGHT = 600;

    // Text interface settings
    final int PIXELS_PER_CHARACTER  = 10;
    final int WINDOW_CHAR_WIDTH     = 80;
    final int WINDOW_CHAR_HEIGHT    = 60;

    // Does the text interface settings override the game settings?
    final boolean WIDTH_OVERRIDE    = true;

    // Internal variables
    int graphicsWidth;
    int graphicsHeight;
    double textRatio;
    boolean frameUpdate = false;
    boolean logicUpdate = false;

    int state;

    // int sleepTme = 0;
    volatile boolean gameRunning = true;
    // double fps = 0;
    // int lastLoopTime = 0;

    JFrame windowFrame;
    JRootPane windowRoot;
    JMenuBar menu;

    // The graphics panel needs to be grabbed every frame.
    Graphics2D graphicsPanel;
    JPanel inputPanel;
    JPopupMenu popup;

    Board board;
    Player player;

    ArrayList<Entity> renderables;

    public Font fontUI;
    public Font fontBoard;
    public Font fontBoardSmall;

    //level currentLevel;
    //LinkedList<level> levels;

    public void firstTimeRun(){
        try {
        //Font font = new Font("resources/Perfect DOS VGA 437.ttf", Font.PLAIN, 36);
            fontUI      = Font.createFont(Font.TRUETYPE_FONT, new File("resources/Perfect DOS VGA 437.ttf")).deriveFont(18f);
            fontBoard   = Font.createFont(Font.TRUETYPE_FONT, new File("resources/Perfect DOS VGA 437.ttf")).deriveFont(17f);

            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            //register the font
            ge.registerFont(fontUI);
            ge.registerFont(fontBoard);


        } catch (IOException e) {
            e.printStackTrace();
        } catch(FontFormatException e) {
            e.printStackTrace();
        }


        // Get the more complicated objects for complex graphics (if needed)
        windowFrame = UI.getFrame();
        windowRoot = windowFrame.getRootPane();


        windowFrame.setLayout(null);
        windowRoot.setLayout(null);

        // Should make the window non-resizable, but currently fails to do so.
        // TODO: Make this actually work.
        windowFrame.setResizable(false);

        //UI.initialise();
        //windowRoot.setDoubleBuffered(true);



        // Remove visual cruft
        // windowFrame.setVisible(false);
        //windowFrame.setUndecorated(true);
        // windowFrame.setVisible(true);

        menu = windowRoot.getJMenuBar();
        inputPanel = (JPanel)windowRoot.getComponent(0);

        // Set the window size
        if(WIDTH_OVERRIDE){
            graphicsWidth   = PIXELS_PER_CHARACTER * WINDOW_CHAR_WIDTH;
            graphicsHeight  = PIXELS_PER_CHARACTER * WINDOW_CHAR_HEIGHT;
        } else {
            graphicsWidth   = WINDOW_WIDTH;
            graphicsHeight  = WINDOW_HEIGHT;
        }

        UI.setWindowSize(graphicsWidth+15,graphicsHeight -20);

        // Disable the menubar at the top
        menu.setEnabled(false);
        menu.setVisible(false);

        // Add buttons to left of interface
        //UI.addButton("Next Level", this::runNextLevel);
        //UI.addButton("Quit", UI::quit);

        // Ratio where 0.0 = no text panel and 1.0 = no graphics panel
        // Somewhat overriden by buttons and their addition
        textRatio = 0.0;
        UI.setDivider(textRatio);

        // Once the window is set up, lets get some of the graphics values
        // Note that resizing the window will not modify this - it will just hide parts!
        //graphicsWidth   = UI.getCanvasHeight();
        //graphicsHeight  = UI.getCanvasWidth();

        // Don't draw graphics immediately.
        UI.setImmediateRepaint(false);

        UI.setKeyListener(this::keyResponder);
        UI.setMouseMotionListener(this::mouseResponder);

        windowFrame.pack();

        UI.getGraphics().setFont(fontUI);

        //UI.setFontSize(20);

        // Run
        this.state = 0;

        Level level = new Level();
        LinkedList<Level> levels = new LinkedList<Level>();
        levels.add(level);
        System.out.println("Two levels created");


        board = new Board(WINDOW_CHAR_WIDTH, WINDOW_CHAR_HEIGHT, PIXELS_PER_CHARACTER, levels, this);

        System.out.println("Board created");


        // Vision control!
        player.act();

        gameRender();
        System.out.println("First render call completed");

        while(gameRunning){

            // Render

            if(logicUpdate){
                processLogic();
                logicUpdate = false;
            }

            if(frameUpdate){
                gameRender();
                frameUpdate = false;
            }

            // try{
            //     Thread.sleep( (lastLoopTime-System.nanoTime() + OPTIMAL_TIME)/1000000 );
            // }
            // catch(Exception e){
            //     // Smething went wrong!
            //     gameRunning = false;
            // }

        }
    }

    public void keyResponder(String input){

        // TODO: make this play nice with holding keys down

        input = formatKey(input);
        System.out.println(input);

        frameUpdate = true;

        //logicUpdate = false;

        if(input.equals("space")){
            board.updateStatus();
        }
        else{
            this.board.clearStatus();
        }

        if(!this.player.intelligent)
            return;



        if(input.equals("p")){
            player.menu = 0;
        }
        else if(input.equals("i")){
            player.menu = 1;
        }
        else if(input.equals("v")){
            // Invocation
            player.build_spell();
            logicUpdate = true;
        }
        if(state == 0){
            if(input.equals("up")){
                // movement north
                player.move(0);
                logicUpdate = true;
            }
            else if(input.equals("down")){
                // movement south
                player.move(2);
                logicUpdate = true;
            }
            else if(input.equals("left")){
                // movement west
                player.move(3);
                logicUpdate = true;
            }
            else if(input.equals("right")){
                // movement east
                player.move(1);
                logicUpdate = true;
            }
        }
        // Spell selection
        if(input.equals("q")){
            if(player.currentSpells.size() > 0)
                player.currentSpellIndex = 0;
        }
        else if(input.equals("w")){
            if(player.currentSpells.size() > 1)
                player.currentSpellIndex = 1;
        }
        else if(input.equals("e")){
            if(player.currentSpells.size() > 2)
                player.currentSpellIndex = 2;
        }
    // public int range; // 0 is personal, n = dist
    // public int targetType; // 0 = buff, 1 = line, 2 = PBAoE
    // public int damageType; // 0 = normal, 1 = penetrating
    // public int statusAff; // 0 = none, 1 = haste, 2 = slow, 3 = poison
    // public int duration; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)
        // Spells!
        else if(input.equals("b") || input.equals("s") || input.equals("t") || input.equals("g") || input.equals("r")){

            if((this.state == 0) && (!(player.currentSpells.size() == 0))){
                // The player is casting a spell!
                this.state = 1;
                // Need to know the sort of spell
                if(player.getCurrentSpell().targetType == 0)
                    board.statusAddHead("Casting a buff! Hit enter to self-cast.");
                else if(player.getCurrentSpell().targetType == 1)
                    board.statusAddHead("Casting a spell! Use arrow keys to select cast direction.");
                else if(player.getCurrentSpell().targetType == 2)
                    board.statusAddHead("Casting a spell! Hit enter to cast.");
                return;
            }

        }

        if(state == 1){
            // TODO: user a trigger variable
            this.state = 0;
            if(input.equals("b")){
                //the player is cancelling their spell
                board.statusAddHead("Spell cancelled.");
            }
            // Targeting spells!
            // Here, we need to know what sort of spell it is
            // Buff
            if(player.getCurrentSpell().targetType == 0){
                if(input.equals("enter")){
                    logicUpdate = true;
                    board.statusAddHead("Buff(?) cast on self.");
                    player.castSpell("b", -1);
                    this.state = 0;
                }
            }
            // PBAoE
            else if(player.getCurrentSpell().targetType == 2){
                if(input.equals("enter")){
                    logicUpdate = true;
                    board.statusAddHead("AoE Spell cast.");
                    player.castSpell("b", -1);
                    this.state = 0;
                }
            }
            // Line based
            else if(player.getCurrentSpell().targetType == 1){
                if(input.equals("up")){
                    logicUpdate = true;
                    player.castSpell("b", 0);
                    this.state = 0;
                }
                else if(input.equals("right")){
                    logicUpdate = true;
                    player.castSpell("b", 1);
                    this.state = 0;
                }
                else if(input.equals("down")){
                    logicUpdate = true;
                    player.castSpell("b", 2);
                    this.state = 0;
                }
                else if(input.equals("left")){
                    logicUpdate = true;
                    player.castSpell("b", 3);
                    this.state = 0;
                }
            }
        }

        //System.out.println(input);
    }

    public void mouseResponder(String action, double x, double y){
        // Fimd out what tile is under the mouse

        if(action.equals("clicked"))
        {
            //System.out.println("Mouse x: " + x + " y: " + y + " action: " + action);
        }

        if(action.equals("pressed"))
        {

        }

        if(action.equals("released"))
        {

        }
    }

    public String formatKey(String input){
        if(input.length() == 0)
            return "";
        return input.toLowerCase();
    }

    public int gameRender(){
        // Clear whatever was previously rendered.
        // This does mean we will need to redraw every object (every frame)!
        UI.clearGraphics();
        graphicsPanel = UI.getGraphics();
        //System.out.println(graphicsPanel);
        //graphicsPanel.setFont(fontUI);

        //graphicsPanel.setLayout(null);

        graphicsPanel.setColor(Color.BLACK);
        UI.fillRect(0, 0, graphicsWidth, graphicsHeight);

        // FPS counter
        //UI.drawString(String.format("FPS: %4.2f", this.fps), 20, 20);

        board.render(graphicsPanel);
        //windowRoot.getContentPane().setLayout(null);
        //UI.setColor(Color.WHITE);

        // As immediate mode is set to false, we will need to explicitly call repaint
        UI.repaintGraphics();

        return 0;
    }

    public int processLogic(){
        //TODO: advance game clock
        for(Entity e : board.level.objects){
            if(e.intelligent)
                e.act();
        }

        return 0;
    }

    public static void main(String[] args){
        Game obj = new Game();
        obj.firstTimeRun();
    }

}




import java.util.Random;
import java.awt.Color;
/**
 * Greed is the element characterised by:
 * high randomness, special effects
 */
public class Greed extends Element
{
    private Random rand;

    public Greed()
    {

        this.name = "Greed";

        this.chargeMin = 1;
        this.chargeMax = 4;
        this.rand = new Random();
        this.charges = rand.nextInt((this.chargeMax - this.chargeMin) + 1) + this.chargeMin;

        this.prefixes   = new String[]{"Greedy", "Gobbling", "Gaining", "Gathering", "Gilded", "Gambler's"};
        this.roots      = new String[]{"Gold", "Gear", "Goblet", "Grail"};
        this.suffixes   = new String[]{"Greed", "Gluttony", "Gambling"};

        this.range      = rand.nextInt((10 - 1) + 1) + 1; // 0 is personal, n = dist
        this.m_range    = 1;
        this.targetType = rand.nextInt((2 - 1) + 1) + 1; // 0 = buff, 1 = line, 2 = PBAoE
        this.damageType = rand.nextInt((1 - 0) + 1) + 0; // 0 = normal, 1 = penetrating
        this.statusAff  = 0; // 0 = none, 1 = haste, 2 = slow, 3 = poison
        this.duration   = rand.nextInt((15 - 0) + 1) + 0; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)
        this.m_duration = 1.4;
        this.damage     = rand.nextInt((75 - 0) + 1) + 0;
        this.m_damage   = rand.nextInt((3 - 0) + 1) + 0;
        this.self_damage = rand.nextInt((5) +2) + 3;
        this.color = Color.YELLOW;
    }

    // Prefix
    public double prefix_mod(){
        return 1.5;
    }

    // Effect
    public double effect_mod(){
        return 1.5;
    }

    // Suffix
    public double suffix_mod(){
        return 1.5;
    }

}







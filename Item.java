

public abstract class Item extends Entity{

    public Item(){
        this.movable      = false;
        this.pushable     = false;
        this.destructable = false;
        this.pathable     = true;
        this.intelligent  = false;
        //this.inworld      = true;
    }

    public abstract void pickup();

    public void render(){}
    public void act(){}
    public boolean applyDamage(int damage){return false;};
}

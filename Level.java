import java.util.ArrayList;
import java.util.Random;
public class Level
{

    public int width;
    public int height;

    public Entity[][] map;
    public Entity[][] backMap;
    public Entity[][] volatileMap;

    public ArrayList<Entity> objects;

    public boolean stairDownGenerated = false;

    public Level()
    {
        this.width  = 140;
        this.height = 140;
        Random rand = new Random();
        //Mapbuilder builder = new Mapbuilder(this.width, this.height, 500);
        Maptester builder = new Maptester(this.width, this.height, rand.nextInt(251)+100, rand.nextInt(3));

        // The data as it changes during gameplay
        this.map        = new Entity[this.width][this.height];
        // The hopefully immutable level background
        this.backMap    = new Entity[this.width][this.height];
        // Transient effects
        this.volatileMap= new Entity[this.width][this.height];

        for(int i = 0; i < this.width; i++){
            for(int j = 0; j < this.height; j++){
                if(builder.map[i][j] == '#'){
                    this.map[i][j]          = new Wall();
                    this.backMap[i][j]      = new Wall();
                    this.volatileMap[i][j]  = null;
                }
                else{
                    this.map[i][j]          = new Floor();
                    this.backMap[i][j]      = new Floor();
                    this.volatileMap[i][j] = null;
                }
            }
        }


        this.objects = new ArrayList<Entity>();
    }

    public void makeLegal(int x, int y){
        int counter = 0;

        // First, cast in each of the 4 directions and find the closest not-wall
        int north   = findFloor(0, x, y);
        int east    = findFloor(1, x, y);
        int south   = findFloor(2, x, y);
        int west    = findFloor(3, x, y);

        int mini = Math.min(Math.min(north, south), Math.min(east, west));

        System.out.printf("Nearest floor is %d tiles away.\n", mini);

        if(mini == north){
            while(true){
                if(this.map[x][y-counter].name.equals("floor"))
                    break;
                this.map[x][y-counter]          = new Floor();
                this.backMap[x][y-counter]      = new Floor();
                this.volatileMap[x][y-counter]  = new Floor();
            }
        }
        if(mini == east){
            while(true){
                if(this.map[x+counter][y].name.equals("floor"))
                    break;
                this.map[x+counter][y]          = new Floor();
                this.backMap[x+counter][y]      = new Floor();
            }
        }
        if(mini == south){
            while(true){
                if(this.map[x][y+counter].name.equals("floor"))
                    break;
                this.map[x][y+counter]          = new Floor();
                this.backMap[x][y+counter]      = new Floor();
            }
        }
        if(mini == west){
            while(true){
                if(this.map[x-counter][y].name.equals("floor"))
                    break;
                this.map[x-counter][y]          = new Floor();
                this.backMap[x-counter][y]      = new Floor();
            }
        }

        System.out.printf("Wall is now legal\n");
    }

    // Ugly but really fast to code
    public int findFloor(int direction, int x, int y){
        int i = 1;
        if(direction == 1){
            while(true){
                if(y - i > 0){
                    i++;
                    if(this.backMap[x][y - i].name.equals("floor"))
                        return i;
                } else{
                    return 9999;
                }
            }
        } else if(direction == 1){
            while(true){
                if(x + i < this.width - 1){
                    i++;
                    if(this.backMap[x + i][y].name.equals("floor"))
                        return i;
                } else{
                    return 9999;
                }
            }
        } else if(direction == 2){
            while(true){
                if(y + i < this.height - 1){
                    i++;
                    if(this.backMap[x][y + i].name.equals("floor"))
                        return i;
                } else{
                    return 9999;
                }
            }
        } else if(direction == 3){
            while(true){
                if(x - i > 0){
                    i++;
                    if(this.backMap[x - 1][y].name.equals("floor"))
                        return i;
                } else{
                    return 9999;
                }
            }
        }
        return i;
    }

}

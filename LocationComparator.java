import java.util.Comparator;

public class LocationComparator implements Comparator<int[]>{
    //public static Comparator<int[]> locationComparator = new Comparator<int[]>() {
    private int posX;
    private int posY;

    public LocationComparator(int x, int y){
        this.posX = x;
        this.posY = y;
    }

    public int compare(int[] s1, int[] s2) {
        return (int)((Math.pow(s1[0] - this.posX, 2) + Math.pow(s1[1] - this.posY, 2)) - (Math.pow(s2[0] - this.posX, 2) + Math.pow(s2[1] - this.posY, 2)));
    }
}

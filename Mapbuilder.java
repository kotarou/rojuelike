import java.util.Random;
import java.util.ArrayList;

public class Mapbuilder {

    private int width;
    private int height;

    private int maxRoomWidth    = 8;
    private int maxRoomHeight   = 8;
    private int minRoomWidth    = 3;
    private int minRoomHeight   = 3;

    // The hallway method will adjust for direction on these
    // private int maxHallWidth    = 8;
    // private int maxHallHeight   = 3;
    // private int minHallWidth    = 1;
    // private int minHallHeight   = 1;

    char[][]map;

    Random rand;

    int candidateLeft   = 0;
    int candidateTop    = 0;
    int candidateWidth  = 0;
    int candidateHeight = 0;

    int features;

    public Mapbuilder(int w, int h, int f){
        this.features = f;
        this.width = w;
        this.height = h;
        this.map = new char[this.width][this.height];
        this.rand = new Random();


        // Write wall characters into the entire base map
        for(int i = 0; i < this.width; i++)
            for(int j = 0; j < this.height; j++)
                this.map[i][j] = '#';

        //this.printMap();
        System.out.println("Initial map seeded");

        for(int b = 0; b < this.features; b++){
            // Find valid positions for a room and place it
            int[] roomValues = new int[]{-1, -1, -1, -1};
            while(roomValues[0] == -1){
                roomValues = attemptRoomCreation();
            }
            this.fillRoom(roomValues, '.');

            //this.printMap();
            System.out.printf("Initial room placed @ (%d, %d), dimensions (%d, %d)\n",
                               roomValues[0], roomValues[1], roomValues[2], roomValues[3]);

            // Cast until a valid door position is found
            int[] validDoorway = new int[]{-1, -1, -1};
            while(validDoorway[0] == -1){
                validDoorway = attemptDoorwayCreation();
            }
            map[validDoorway[0]][validDoorway[1]] = '.';

            //this.printMap();
            System.out.println("Initial doorway placed");

            // Create a corridoor!
            // Cast until a valid door position is found
            int[] validHallway = new int[]{-1, -1, -1, -1};
            int attempts = 0;
            while(validHallway[0] == -1){
                validHallway = attemptHallwayCreation(validDoorway);
                attempts++;
                if(attempts > 30)
                    break;
            }
            this.fillRoom(validHallway, '.');
            //this.printMap();
            //System.out.printf("Initial hallway placed @ (%d, %d), dimensions (%d, %d)\n",
            //                   validHallway[0], validHallway[1], validHallway[2], validHallway[3]);
        }


        for(int c = 0; c < this.width; c++){
            this.map[c][0] = '#';
            this.map[c][this.height -1] = '#';
        }
        for(int d = 0; d < this.height; d++){
            this.map[0][d] = '#';
            this.map[this.width-1][d] = '#';
        }

        //this.printMap();

        //return this;

    }
    public int[] attemptHallwayCreation(int[] params){
        int doorX       = params[0];
        int doorY       = params[1];
        int direction   = params[2];

        // Heres a mindscrew: the direction of the hallway swaps max w and max h around
        // A hallway going up should be tall and narrow
        // A hallway going along should be fat and short
        int w; int h;
        if(direction % 2  == 0){
            w = this.random(1, 3);
            h = this.random(2, 9);
        } else {
            w = this.random(2, 9);
            h = this.random(1, 3);
        }

        int x = 0;
        int y = 0;

        // Given the direction and the doorway location, compute x, y (left, top)
        // There is some weirdness with the directions here
        // rotate them all 90
        switch(direction){
            case 1: //NOW EAST -NORTH
                x = doorX - (int)Math.floor(w / 2);
                y = doorY - h;
                break;
            case 2: //NOW SOUTH - EAST
                x = doorX;
                y = doorY - (int)Math.floor( h / 2);
                break;
            case 3: //NOW WEST -SOUTH
                x = doorX - (int)Math.floor(w / 2);
                y = doorY;
                break;
            case 4: //NOW NORTH -WEST
                x = doorX + w;
                y = doorY - (int)Math.floor(h / 2);
                break;
        }

        System.out.printf("The doorway is at at (%d, %d) direction: %d\n", doorX, doorY, direction);
        System.out.printf("Attempting to place hallway at (%d, %d) with dimensions (%d, %d)\n", x, y, w, h);

        // Is the computation valid?
        // Initial sanity check
        if( x < 0 || y < 0 || x > this.width-2 || y > this.height - 2)
            return new int[]{-1, -1, -1, -1};
        else
            for(int i = 0; i < w; i++)
                for(int j = 0; j < h; j++)
                    if(this.map[x+i][y+j] == '.')
                        return new int[]{-1, -1, -1, -1};

        // It is valid!
        return new int[]{x, y, w, h};
    }


    /**
    *   Returns an array [x, y, direction]
    **/
    public int[] attemptDoorwayCreation(){
        int x = this.random(1, this.width - 2);
        int y = this.random(1, this.height - 2);

        // 4 cases for room: NESW
        // NORTH
        if(y > 0 && this.map[x][y-1] == '.' && this.map[x][y] != '.')
            return new int[]{x, y, 0};
        // EAST
        if(x < this.width - 2 && this.map[x+1][y] == '.' && this.map[x][y] != '.')
            return new int[]{x, y, 1};
        // SOUTH
        if(y < this.height - 2 && this.map[x][y+1] == '.' && this.map[x][y] != '.')
            return new int[]{x, y, 2};
        // WEST
        if(x > 0 && this.map[x-1][y] == '.' && this.map[x][y] != '.')
            return new int[]{x, y, 3};

        // Nope, invalid!
        // this.map[x][y] = '0';
        return new int[]{-1, -1, -1};
    }



    public int[] attemptRoomCreation(){
        // (x, y) are the coordinates of the top left corner of the room
        // (w, h) are the room bounds
        int w = this.random(this.minRoomWidth, this.maxRoomWidth);
        int h = this.random(this.minRoomHeight, this.maxRoomHeight);

        // Generate a valid point, considering the size of the generated room
        int x = this.randi(this.width - (w + 1));
        int y = this.randi(this.height - (h + 1));

        // Sanity check on return statement
        if (x + w < this.width && y + h < this.height && x >= 0 && y >= 0){
            return new int[]{x, y, w, h};
        }

        return new int[]{-1, -1, -1, -1};
    }

    public void fillRoom(int[] parameters, char fill){
        int left    = parameters[0];
        int top     = parameters[1];
        int width   = parameters[2];
        int height  = parameters[3];

        for(int i = 0; i < width; i++)
            for(int j = 0; j < height; j++)
                this.map[left+i][top+j] = fill;
    }

    public int random(int min, int max){
        return this.rand.nextInt((max - min) + 1) + min;
    }

    public int randi(int max){
        return this.random(0, max);
    }

    public void printMap(){
        System.out.println(" ");
        for (int i = 0; i < this.width; i++){
            for (int j = 0; j < this.height; j++){
                // if( j == 0 )
                //     System.out.print(i);
                System.out.print(this.map[i][j]);
            }
            System.out.println(' ');
        }
    }

}




 /*   // instance variables - replace the example below with your own
    private int COLX = 30;
    private int ROWY = 30;
    char[][] map;
    // make 6 more rooms!
    int FEATURES = 100;


    public mapbuilder() {
        // initialise instance variables
        map = new char [COLX][ROWY];
        // first we fill with wall
        System.out.println("Started");

        for (int i = 0; i < COLX; i++){
            for (int ii = 0; ii < ROWY; ii++){
                this.map[i][ii] = '#';
            }
        }

        System.out.println("Filled Room");
        this.printmap();

        // we pick a random spot and try and put a room there
        boolean room = false;
        int x=0;
        int y=0;
        while (!room){
            x = this.getrandom(0, COLX-1);
            y = this.getrandom(0, ROWY-1);
            System.out.println("X IS " + x + " Y IS " + y);
            //int topleftx, int toplefty, int botrightx, int botrighty, int centerx, int centery, boolean draw
            room = makeroom(-1,-1,2,2,x,y,false);
        }
        room = makeroom(-1,-1,2,2,x,y,true);

        this.printmap();

        int curfeatures = 0;
        boolean nodoor = true;
        int doorx=1;
        int doory=1;
        boolean invalid = false;
        int corridorlength;
        Direction direction = Direction.INVALID;
        Feature randomfeature;
        Random rgen = new Random();
        int rn;


        while (direction == Direction.INVALID){
            doorx = getrandom(1, COLX-1);
            doory = getrandom(1, ROWY-1);
            System.out.println("X IS " + doorx + " Y IS " + doory);
            direction = this.checkadjacent(doorx, doory);
            if (direction != Direction.INVALID){
               this.map[doorx][doory] = 'E';
               //this.printmap();
            }
        }


        corridorlength=4;

        if (this.buildcorridor(direction, corridorlength, doorx, doory, false) == true){
                 this.buildcorridor(direction, corridorlength, doorx, doory, true);
                 this.printmap();
        }

        else{
            //System.out.println("failed at, " + doorx + ", " + doory);
            this.printmap();
        }

        }



    // for the first room only!
    public boolean makeroom(int topleftx, int toplefty, int botrightx, int botrighty, int centerx, int centery, boolean draw){
        for (int i = (topleftx + centerx); i < (botrightx + centerx); i++){
            for (int ii = (toplefty + centery); ii < (botrighty + centery); ii++){
                if ((i <= 0 ) || (i > (COLX-2))){
                    return false;
                }

                if ((ii <= 0 ) || ( ii > (ROWY-2))){
                    return false;
                }
                if (draw == true){
                    this.map[i][ii] = '.';
                }
            }
        }
        return true;
    }


    // returns true IFF there is one floor tile adjacent to the spot
    public Direction checkadjacent(int x, int y){
        int count = 0;
        Direction dir = Direction.INVALID;

        if ((x <= 0) || (x >= (COLX-2)) || (y <= 0) || (y >= (ROWY-2))){
            return Direction.INVALID;
        }

        // we put in the opposite of the point, since that is what needs checking
        if (this.map[x][y-1] == '.'){
            dir = Direction.UP;
            count++;
        }
        if (this.map[x][y+1] == '.'){
            dir = Direction.DOWN;
            count++;
        }
        if (this.map[x-1][y] =='.'){
            dir = Direction.LEFT;
            count++;

        }
        if (this.map[x+1][y] == '.'){
            dir = Direction.RIGHT;
            count++;
        }

        if ((count == 0) || (count > 1)){
            dir = Direction.INVALID;
        }
    if (dir != Direction.INVALID){
        System.out.println("returning " + dir);
    }
    //this.map[x][y] = 'O';
    return dir;
    }

    public enum Direction {
    LEFT, RIGHT, UP, DOWN, INVALID
    }

    public enum Feature {
        CORRIDOR, ROOM
    }

       private boolean buildcorridor(Direction direction, int length, int startx, int starty, boolean writetomap){

        // check obvious bounds issues first
        // this is safety for drawing larger rooms,
        // since we only extend down one axis at this stage, we don't need to recheck the start position on both axis
        // but we do have to check the side we are not extending in for a multisized room, as that could be out of bounds, but will be for all points if any

        if ((startx <= 1) || (startx >= (ROWY-1)) || (starty <= 1) || (starty >= (COLX-1))){
            return false;
        }

        System.out.println(direction);
        switch (direction){
            case UP:
                for (int i = 1; i < (length + 1); i++){
                    if ((starty + i) >= (ROWY-1)){
                        System.out.println("failed at first clausesty " + starty + " at i " + i);
                        this.map[startx][starty+i] = 'X';
                        return false;
                    }

                    if (this.map[startx][starty+i] == '.'){
                        System.out.println("already occupied at " + startx + " " + (starty+i));
                        this.map[startx][starty+i] = 'X';
                        return false;
                    }

                    if (writetomap == true){
                        System.out.println("drawing pt at " + startx + " " + (starty+i));
                        this.map[startx][starty+i] = '.';
                    }

                }

            case DOWN:
                for (int i = 1; i < (length + 1); i++){
                    if ((starty - i) <= 1){
                        System.out.println("failed at first clause sty " + starty + " at i " + i);
                        this.map[startx][starty-i] = 'X';
                        return false;

                    }

                    if (this.map[startx][starty-i] == '.'){
                        System.out.println("already occupied at " + startx + " " + (starty-i));
                        this.map[startx][starty-i] = 'X';
                        return false;
                    }

                    if (writetomap == true){
                            System.out.println("drawing pt at " + startx + " " + (starty-i));
                            this.map[startx][starty-i] = '.';
                    }

                }

            case LEFT:
                for (int i = 1; i < (length + 1); i++){
                    if ((startx + i) >= (COLX-1)){
                        System.out.println("failed at first clause stx " + startx + " at i " + i);
                        this.map[startx+1][starty] = 'X';
                        return false;

                    }

                    if (this.map[startx+1][starty] == '.'){
                        System.out.println("already occupied at " + (startx+1) + " " + starty);
                        this.map[startx+1][starty] = 'X';
                        return false;
                    }

                    if (writetomap == true){
                            System.out.println("drawing pt at " + (startx+1) + " " + starty);
                            this.map[startx+1][starty] = '.';
                    }
                }

            case RIGHT:
                for (int i = 1; i < (length + 1); i++){
                    if ((startx - i) <= 1){
                        System.out.println("failed at first clause stx " + startx + " at i " + i);
                        this.map[startx-i][starty] = 'X';
                        return false;
                    }

                    if (this.map[startx-1][starty] == '.'){
                        System.out.println("already occupied at " + (startx-1) + " " + starty);
                        this.map[startx-i][starty] = 'X';
                        return false;
                    }

                    if (writetomap == true){
                            System.out.println("drawing pt at " + (startx-1) + " " + starty);
                            this.map[startx-i][starty] = '.';
                    }
                }
        }
        this.map[startx][starty] = '.';
        return true;
    }

    public static int getrandom(int min, int max) {
        Random rgen = new Random();
        int rand = rgen.nextInt((max - min) + 1) + min;
        return rand;
    }

    public void printmap(){
        for (int i = 0; i < ROWY; i++){
                for (int ii = 0; ii < COLX; ii++){
                    System.out.print(this.map[ii][i]);
                }
                System.out.println("");
        }
    }
}*/

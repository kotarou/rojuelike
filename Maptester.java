import java.util.Random;
import java.util.HashSet;
import java.util.Iterator;
/**
 * Write a description of class Maptester here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Maptester
{
    public int width   = 50;
    public int height  = 50;
    private int limit = 300;

    public char[][] map;
    private HashSet<int[]> borders;
    Random rand;

    public int genType;


    public Maptester(int ww, int hh, int ff, int generationType)
    {
        System.out.printf("MAP GENERATING: FEATURES %d, TYPE %d\n", ff, generationType);
        // Setup
        this.rand = new Random();
        this.borders = new HashSet<int[]>();

        // Hard coded for now because they are cooler
        this.genType = 1; //generationType;

        this.width = ww;
        this.height = hh;
        this.limit = (this.genType  == 0 ? ff : (int)(ff / 3));

        // Fill the map with stone
        this.map = new char[this.width][this.height];

        for(int i = 0; i < this.width; i++)
            for(int j = 0; j < this.height; j++)
                this.map[i][j] = '#';

        System.out.printf("Map is stoned\n");


            // Make a random room in the middle
            this.addRoom(this.width/2 - 3, this.height/2 - 2, 5, 5, -1);
            //this.printMap();

            System.out.printf("Map has one room\n");

        for(int i = 0; i < this.limit; i++){
            // Pick a random border of the generated rooms
            int[] chosenBorder = this.getSetElem();
            this.borders.remove(chosenBorder);

            //System.out.printf("Border selected: (%d, %d): %d\n", chosenBorder[0], chosenBorder[1], chosenBorder[2]);

            int w = 0; int h = 0; boolean added = false; int attempts = 0;
            // Attach an element to it
            while(!added){
                if(attempts > 30)
                    break;
                switch(this.random(0,1)){
                    case 0: // Add another room
                        w = this.random(3, 5);
                        h = this.random(3, 5);
                        if(this.genType == 1){
                            // Give it a chance to reorient room
                            switch(this.random(0,3)){
                                case 0:
                                    // Normal TL corner
                                    break;
                                case 1:
                                    // Render from TR corner
                                    chosenBorder[0] = chosenBorder[0] - (w + 1);
                                    break;
                                case 2:
                                    // Render from BR corner
                                    chosenBorder[0] = chosenBorder[0] - (w + 1);
                                    chosenBorder[1] = chosenBorder[1] - (h + 1);
                                    break;
                                case 3:
                                    // Render from BL corner
                                    chosenBorder[1] = chosenBorder[1] - (h + 1);
                                    break;
                            }
                        }
                        added = this.addRoom(chosenBorder[0], chosenBorder[1], w, h, chosenBorder[2]);
                        attempts++;
                    break;
                    case 1: // Add a hallway
                        w = this.random(1, 8);
                        h = this.random(1, 3);
                        if(this.genType == 1){
                            // Give it a chance to reorient hall
                            switch(this.random(0,3)){
                                case 0:
                                    // Normal TL corner
                                    break;
                                case 1:
                                    // Render from TR corner
                                    chosenBorder[0] = chosenBorder[0] - w;
                                    break;
                                case 2:
                                    // Render from BR corner
                                    chosenBorder[0] = chosenBorder[0] - w;
                                    chosenBorder[1] = chosenBorder[1] - h;
                                    break;
                                case 3:
                                    // Render from BL corner
                                    chosenBorder[1] = chosenBorder[1] - h;
                                    break;
                            }
                        }
                        added = this.addHallway(chosenBorder[0], chosenBorder[1], w, h, chosenBorder[2]);
                        attempts++;
                    break;
                }
            }
            //this.addRoom(chosenBorder[0], chosenBorder[1]);
            //this.printMap();

            //System.out.printf("Map has more elements (%d)\n", i);
        }

        // for(int i = 0; i < this.width; i++){
        //     this.map[i][0] = '#';
        //     this.map[i][this.height - 1] = '#';
        // }
        // for(int j = 0; j < this.width; j++){
        //     this.map[0][j] = '#';
        //     this.map[this.width - 1][j] = '#';
        // }

    }

    public int[] getSetElem(){
        int index = rand.nextInt(this.borders.size());
        Iterator<int[]> iter = this.borders.iterator();
        for (int i = 0; i < index; i++) {
            iter.next();
        }
        return iter.next();
    }

    public int random(int min, int max){
        return this.rand.nextInt((max - min) + 1) + min;
    }

    public boolean addHallway(int x, int y, int w, int h, int direction){
        // If the hallway comes out the sides, flip the width and height
        if(direction == 1 || direction == 3)
            return this.addRoom(x, y, h, w, direction);
        else
            return this.addRoom(x, y, w, h, direction);
    }

    public boolean addRoom(int x, int y, int w, int h, int direction){
        // Create the room itself and add the broders to the border set
        boolean diff        = false;
        boolean borderDiff  = false;
        for(int i = 0; i < w; i++)
            for(int j = 0; j < h; j++){
                int dx = 0;
                int dy = 0;
                switch(direction){
                    case 0:
                        dx = i;
                        dy = -(j + 1);
                        break;
                    case 1:
                        dx = i + 1;
                        dy = j;
                        break;
                    case 2:
                        dx = i;
                        dy = j + 1;
                        break;
                    case 3:
                        dx = -(i + 1);
                        dy = j;
                        break;
                    case -1:
                        dx = i;
                        dy = j;
                        break;
                }
                if(x+dx < this.width && y+h < this.height && y+dy > -1 && x+dx > -1){
                    if(this.map[x+dx][y+dy] == '.')
                        return false;
                    diff = true;
                    if( this.borders.remove(new int[]{x+dx, y+dy, 0}) ||
                        this.borders.remove(new int[]{x+dx, y+dy, 1}) ||
                        this.borders.remove(new int[]{x+dx, y+dy, 2}) ||
                        this.borders.remove(new int[]{x+dx, y+dy, 3}) ) {
                        // This addition is a border change
                        borderDiff = true;

                    }
                    this.map[x+dx][y+dy] = '.';
                    // Slow and cheeky

                    if(dx == 0)
                        this.borders.add(new int[]{x+dx, y+dy, 3});
                    else if (dy == 0)
                        this.borders.add(new int[]{x+dx, y+dy, 0});
                    else if (dx == w - 1)
                        this.borders.add(new int[]{x+dx, y+dy, 1});
                    else if (dy == h - 1)
                        this.borders.add(new int[]{x+dx, y+dy, 2});
                }

        }

        // diff will indicate if we added a valid room or not
        // if borderDiff is false, we never intersected another room
        // expand the room by one in each direction
        for(int i = -1; i <= w; i++){
            if(i + x > 0 && i + x < this.width - 1){
                if(y - 1 > 0){
                    this.map[x+i][y-1] = '.';
                    this.borders.add(new int[]{x+i, y-1, 0});
                }
                if(y + h + 1 < this.height - 1 && y + h + 1 > 0){
                    this.map[x+i][y+h+1] = '.';
                    this.borders.add(new int[]{x+i, y+h+1, 2});
                }
            }
        }

        for(int j = -1; j <= h; j++){
            if(j + y > 0 && j + y < this.height - 1){
                if(x - 1 > 0){
                    this.map[x-1][y+j] = '.';
                    this.borders.add(new int[]{x-1, y+j, 0});
                }
                if(x + w + 1 < this.width - 1 && w + x + 1 > 0){
                    this.map[x+w+1][y+j] = '.';
                    this.borders.add(new int[]{x+w+1, y+j, 2});
                }
            }
        }
        return diff;

    }

    public void printMap(){
        System.out.println(" ");
        for (int i = 0; i < this.width; i++){
            for (int j = 0; j < this.height; j++){
                // if( j == 0 )
                //     System.out.print(i);
                System.out.print(this.map[i][j]);
            }
            System.out.println(' ');
        }


        // for(int[] ele : this.borders){
        //     System.out.printf("(%d, %d), ", ele[0], ele[1]);
        // }

    }


}

import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
/**
 * Write a description of class Player here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Player extends Creature
{
    public String status;

    public ArrayList<Spell> currentSpells;
    public ArrayList<Element> inventory;

    public int currentSpellIndex;

    public boolean[][] visible;

    public int menu = 1;

    public int lightRadiusSquared = 16;
    public int maxVisionSquared = 400;
    public boolean scaryLights = false;

    public ArrayList<Status> statusList;

    /**
     * Constructor for objects of class Player
     */
    public Player(Board board)
    {
        this.character = "@";
        this.color = Color.YELLOW;
        this.htmlColor = "#ffff00";

        this.name = "player";

        this.visible = new boolean[board.level.width][board.level.height];

        this.posX = board.level.width / 2;
        this.posY = board.level.height / 2;
        //this.status = "Normal";
        this.statusList = new ArrayList<Status>();
        this.statusList.add(new Status(Status.condition.NORMAL, -1));
        this.statusList.add(new Status(Status.condition.XRAY, -1));

        this.board = board;

        for(int i = 0; i < board.level.width; i++)
            for(int j = 0; j < board.level.height; j++)
                this.visible[i][j] = true;

        this.hp = 400.0;
        this.maxHp = 100.0;

        this.currentSpells = new ArrayList<Spell>();
        this.inventory = new ArrayList<Element>(3);

        // Give the player a single cast of a spell to start
        this.currentSpells.add(new Spell(new Blade(),   new Blade(),        new Blade(), 1));
        this.currentSpells.add(new Spell(new Greed(),   new Greed(),        new Greed(), 2));
        this.currentSpellIndex = 0;

        this.inventory.add(new Time());
        this.inventory.add(new Radiation());
        this.inventory.add(new Stitch());

        this.movable = true;
        this.intelligent = true;
    }

    public String buildStatusString(){
        String hpColor = "green";
        if( this.hp <= 0.0 || this.maxHp / this.hp >= 10.0 )
            hpColor = "red";
        else if( this.maxHp / this.hp >= 3.0 )
            hpColor = "yellow";

        String HP = "<span style='color: white;'>HP: (</span>"+"<span style='color:"+hpColor+";'>"+(int)this.hp+"</span><span style='color: white;'>/"+this.maxHp+") </span>";
        String c1 = "<span style='color: white;'>In</span><span style='color: green;'>v</span><span style='color: white;'>oke </span>";
        //String c3 = "<span style='color: white;'>Status: " + this.status + "</span>";
        String c3 = "<span style='color: white;'>Status: ";
        for(Status s : this.statusList){
            if(s.enabled)
                c3 = c3 + s.toString();
        }
        c3 = c3 + "</span>";
        return HP + c1 + c3;
    }

    // public int[] sees(int x, int y){
    //     // No need for a sanity check
    //     ArrayList<int[]> bLine = BresenhamLine(this.posX, this.posY, x, y);
    //     // Use this only for super scary mode
    //     // for(int[] item : bLine){
    //     //     if(this.board.map[item[0]][item[1]].character.equals("#"))
    //     //         return false;
    //     // }
    //     return true;
    // }

    // Returns the list of points from (x0, y0) to (x1, y1)
    private ArrayList<int[]> BresenhamLine(int x0, int y0, int x1, int y1) {
        int t = 0;
        ArrayList<int[]> result = new ArrayList<int[]>();

        boolean steep = Math.abs(y1 - y0) > Math.abs(x1 - x0);
        if (steep) {
            t = x0; x0 = y0; y0 = t;
            t = x1; x1 = y1; y1 = t;
        }
        if (x0 > x1) {
            t = x0; x0 = x1; x1 = t;
            t = y0; y0 = y1; y1 = t;
        }

        int deltax = x1 - x0;
        int deltay = Math.abs(y1 - y0);
        int error = 0;
        int ystep;
        int y = y0;

        if (y0 < y1)
            ystep = 1;
        else
            ystep = -1;

        for (int x = x0; x <= x1; x++) {
            if (steep)
                result.add(new int[]{y, x});
            else
                result.add(new int[]{x, y});
            error += deltay;
            if (2 * error >= deltax) {
                y += ystep;
                error -= deltax;
            }
        }

        return result;
    }




    public String buildSpellListString(){
        String out = "";
        int i = 0;
        char[] shortcuts = new char[]{'q', 'w', 'e'};
        for(Spell s : this.currentSpells){
            String colo = (i == this.currentSpellIndex ? "white" : "green");
            out = out + "<span style='color: "+colo+";'>"+shortcuts[i]+"</span><span style='color: white;'>.</span>";
            out = out + s.toSmallHTMLString() + "<br />";
            i++;
        }
        return out;
    }

    public String buildInventoryString(){
        String out = "";
        int i = 1;
        for(Element e : this.inventory){
            out = out + "<span style='color: white;'>"+e.name+"</span><br />";
            i++;
        }
        return out;
    }

    public String buildCurrentSpellString(){
        if (this.currentSpells.isEmpty()){
             return "<span style='color: red;'>No Spells: </span>";
        }
        return "<span style='color: white;'>Current spell: </span>" + this.currentSpells.get(this.currentSpellIndex).toHTMLString();
    }

    // Spell handling
    public boolean build_spell(){
        if (this.inventory.size() < 3){
            this.board.statusAddHead("Spell invocation failed as you have insufficient power...");
            return false;
        }

        this.currentSpells.add(new Spell(this.inventory.get(0),this.inventory.get(1), this.inventory.get(2)));
        this.inventory.clear();
        this.board.statusAddHead("Spell invocation successful!");
        return true;

    }

    public Spell getCurrentSpell(){
        return this.currentSpells.get(this.currentSpellIndex);
    }

    public boolean castSpell(String trigger, int direction){
        // The spell had to be the current spell
        // TODO: make sure!

        // Not strictly required, but safe
        if (this.currentSpells.isEmpty()){
            return false;
        }

        Spell current = this.currentSpells.get(this.currentSpellIndex);

        System.out.println(current.targetType);
    // public int range; // 0 is personal, n = dist
    // public int targetType; // 0 = buff, 1 = line, 2 = PBAoE
    // public int damageType; // 0 = normal, 1 = penetrating
    // public int statusAff; // 0 = none, 1 = haste, 2 = slow, 3 = poison
    // public int duration; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)

        // for spells with a HP cost
        if(current.self_damage != 0){
            board.statusAddHead("Spell did " + current.self_damage + " damage to you.");
        }
        this.applyDamage(current.self_damage);

        if(current.targetType == 0){
            // Its a buff

            // Temp effect: give ourselves xray vision
            this.statusList.add(new Status(Status.condition.XRAY, 3));


        }

        else if(current.targetType == 2){
            // Its a radius!
            int ySym = 0;
            int xSym = 0;
           // for(int i = 0; i < current.range; i++)
           //     for(int j = 0; j < current.range; j++)

            for (int x = posX - current.range; x <= posX; x++){
                for (int y = posY - current.range; y <= posY; y++){
                    // we don't have to take the square root, it's slow
                    if ((x - posX)*(x - posX) + (y - posY)*(y - posY) <= current.range*current.range){
                        xSym = posX - (x - posX);
                        ySym = posY - (y - posY);
                        if (x >= 0 && x < this.board.level.width && y >= 0 && y < this.board.level.height) this.board.level.volatileMap[x][y] = new SpellEffect("=" ,current.color, "ff0000");
                        if (x >= 0 && x < this.board.level.width && y >= 0 && y < this.board.level.height) this.board.level.volatileMap[x][ySym] = new SpellEffect("=" ,current.color, "ff0000");
                        if (x >= 0 && x < this.board.level.width && y >= 0 && y < this.board.level.height) this.board.level.volatileMap[xSym][y] = new SpellEffect("=" ,current.color, "ff0000");
                        if (x >= 0 && x < this.board.level.width && y >= 0 && y < this.board.level.height) this.board.level.volatileMap[xSym][ySym] = new SpellEffect("=" ,current.color, "ff0000");
                        // (x, y), (x, ySym), (xSym , y), (xSym, ySym) are in the circle
                    }
                }
            }

    }

        else if(current.targetType == 1){

            if(direction == 0){
                // NORTH
                for(int j = this.posY - 1 ; j >= Math.max(0, this.posY - current.range); j--){
                    if (this.posX >= 0 && this.posX < this.board.level.width && j >= 0 && j < this.board.level.height) this.board.level.volatileMap[this.posX][j] = new SpellEffect("*" ,current.color, "0000ff");
                    if(!current.impact(this.board.level.map[this.posX][j], board)){ break; }

                }
            }
            if(direction == 1){
                // EAST
                for(int i = this.posX + 1 ; i < Math.min(this.posX + current.range, this.board.level.width); i++){
                    if (i>= 0 && i < this.board.level.width && this.posY >= 0 && this.posY < this.board.level.height) this.board.level.volatileMap[i][this.posY] = new SpellEffect("*" ,current.color, "0000ff");
                    if(!current.impact(this.board.level.map[i][this.posY], board)){ break; }

                }
            }
            if(direction == 2){
                // SOUTH
                for(int j = this.posY + 1 ; j < Math.min(this.posY + current.range, this.board.level.height); j++){
                   if (this.posX >= 0 && this.posX < this.board.level.width && j >= 0 && j < this.board.level.height) this.board.level.volatileMap[this.posX][j] = new SpellEffect("*" ,current.color, "0000ff");
                    if(!current.impact(this.board.level.map[this.posX][j], board)){ break; }
                }
            }
            if(direction == 3){
                // WEST
                for(int i = this.posX - 1; i > Math.max(this.posX - current.range, 0); i--){
                    if (i>= 0 && i < this.board.level.width && this.posY >= 0 && this.posY < this.board.level.height) this.board.level.volatileMap[i][this.posY] = new SpellEffect("*" ,current.color, "0000ff");
                    if(!current.impact(this.board.level.map[i][this.posY], board)){ break; }
                }
            }
        }
        /* KARRAYYZZZEE HOURGLASS AOE
        else if(current.targetType == 2){
            // Its a radius!
            for(int i = 0; i < current.range; i++){
                for(int j = 0; j < current.range; j++){
                    if( i == 0 && j == 0)
                        continue;
                    //if( Math.pow(i, 2) + Math.pow(j, 2) < Math.pow(current.range, 2) ){
                        //if(this.posX - i >= 0 && this.posY - j >= 0)

                    this.board.level.volatileMap[this.posX-i][this.posY-j] = new SpellEffect("=" ,Color.RED, "ff0000");
                    this.board.level.volatileMap[this.posX+i][this.posY+j] = new SpellEffect("=" ,Color.RED, "ff0000");

                    current.impact(this.board.level.map[this.posX-i][this.posY-j], board);
                            //if(this.board.map[this.posX-i][this.posY-j].applyDamage(current.damage))
                            //    this.board.statusAddHead("You hit " + this.board.map[this.posX-i][this.posY-j].name + " for " + current.damage + " damage");
                        //if(this.posX + i < this.board.level.width - 1 && this.posY - j < this.board.level.height - 1)
                    current.impact(this.board.level.map[this.posX+i][this.posY+j], board);
                            //if(this.board.map[this.posX+i][this.posY+j].applyDamage(current.damage))
                                //this.board.statusAddHead("You hit " + this.board.map[this.posX+i][this.posY+j].name + " for " + current.damage + " damage");
                    //}
                }
            }

        }
        // square spell?!
        else if(current.targetType == 2){
            // Its a radius!
            for(int i = 0; i < current.range; i++){
                for(int j = 0; j < current.range; j++){
                    if( i == 0 && j == 0)
                        continue;
                    //if( Math.pow(i, 2) + Math.pow(j, 2) < Math.pow(current.range, 2) ){
                        //if(this.posX - i >= 0 && this.posY - j >= 0)

                    this.board.level.volatileMap[this.posX-i][this.posY-j] = new SpellEffect("=" ,Color.RED, "ff0000");
                    this.board.level.volatileMap[this.posX+i][this.posY+j] = new SpellEffect("=" ,Color.RED, "ff0000");
                    this.board.level.volatileMap[this.posX+i][this.posY-j] = new SpellEffect("=" ,Color.RED, "ff0000");
                    this.board.level.volatileMap[this.posX-i][this.posY+j] = new SpellEffect("=" ,Color.RED, "ff0000");
                    current.impact(this.board.level.map[this.posX-i][this.posY-j], board);
                    current.impact(this.board.level.map[this.posX+i][this.posY-j], board);
                    current.impact(this.board.level.map[this.posX-i][this.posY+j], board);
                            //if(this.board.map[this.posX-i][this.posY-j].applyDamage(current.damage))
                            //    this.board.statusAddHead("You hit " + this.board.map[this.posX-i][this.posY-j].name + " for " + current.damage + " damage");
                        //if(this.posX + i < this.board.level.width - 1 && this.posY - j < this.board.level.height - 1)
                    current.impact(this.board.level.map[this.posX+i][this.posY+j], board);
                            //if(this.board.map[this.posX+i][this.posY+j].applyDamage(current.damage))
                                //this.board.statusAddHead("You hit " + this.board.map[this.posX+i][this.posY+j].name + " for " + current.damage + " damage");
                    //}
                }
            }

        }
        */

        current.charges -= 1;

        // if we use the last charge, no more batteries
        if (current.charges == 0){
            this.currentSpells.remove(this.currentSpellIndex);

            // set it to 0, not sure this is ideal, but its the safest
            this.currentSpellIndex = 0;
        }
        return true;
    }

    // Inventory Management
    public boolean pickup_component(Element new_item){
        board.statusAddHead("Elemental ower picked up!");
        if (inventory.size() > 2){
                inventory.remove(0);
        }
        inventory.add(new_item);
        return true;
    }


    public boolean drop_component(int slot){
        if ((slot < 0) || (slot > 2)){
            System.out.println("attempted to drop item at invalid slot " + slot);
            return false;
        }

        if (inventory.size() < (slot-1)){
            System.out.println("attempted to drop item at empty slot " + slot);
            return false;
        }

        inventory.remove(slot);


        //TODO: put the item back on the ground
        return true;

    }

    public void die(){
        this.board.statusAddHead("YOU DIED");
        this.color = Color.RED;
        this.htmlColor = "#ff0000";

        this.movable = false;
        this.intelligent = false;
    }


    public void move(int direction){
        //super.move(direction);
        // NORTH
        int distance = 1;
        if (this.movable == false){
            return;
        }

        if(direction == 0 && this.board.allowsMove(this.posX, this.posY - 1)){
            this.board.offsetY += 1;
            this.board.move(this.posX, this.posY, this.posX, this.posY - 1);
            this.posY -= distance;
        }
        else if (direction == 0 && (this.board.allowsAttack(this.posX, this.posY - 1) != null)){
            this.attack(this.board.allowsAttack(this.posX, this.posY - 1));
        }
        // EAST
        else if(direction == 1 && this.board.allowsMove(this.posX + 1, this.posY)){
            this.board.offsetX -= 1;
            this.board.move(this.posX, this.posY, this.posX + 1, this.posY);
            this.posX += distance;
        }
        else if (direction == 1 && (this.board.allowsAttack(this.posX + 1, this.posY) != null)){
            this.attack(this.board.allowsAttack(this.posX + 1, this.posY));
        }
        // SOUTH
        else if(direction == 2 && this.board.allowsMove(this.posX, this.posY + 1)){
            this.board.offsetY -= 1;
            this.board.move(this.posX, this.posY, this.posX, this.posY + 1);
            this.posY += distance;
        }
        else if (direction == 2 && (this.board.allowsAttack(this.posX, this.posY + 1) != null)){
            this.attack(this.board.allowsAttack(this.posX, this.posY + 1));
        }

        // WEST
        else if(direction == 3 && this.board.allowsMove(this.posX - 1, this.posY)){
            this.board.offsetX += 1;
            this.board.move(this.posX, this.posY, this.posX - 1, this.posY);
            this.posX -= distance;
        }
        else if (direction == 3 && (this.board.allowsAttack(this.posX - 1, this.posY) != null)){
            this.attack(this.board.allowsAttack(this.posX-1, this.posY));
        }



    }

    public void act(){
        for(int i = 0; i < this.board.level.width; i++)
            for(int j = 0; j < this.board.level.width; j++)
                this.visible[i][j] = true;

        int vision = this.lightRadiusSquared;

        if(this.checkBuff(Status.condition.XRAY))
            vision *= 50;

        // Update player visibility matrix
        for(int i = 0; i < this.board.level.width; i++){
            for(int j = 0; j < this.board.level.height; j++){
                if(Math.pow(this.posX - i, 2) + Math.pow(this.posY - j, 2) > this.maxVisionSquared){
                    this.visible[i][j] = false;
                }
                else if(this.visible[i][j] && !(i == this.posX && j == this.posY))
                {
                    boolean occluded = false;

                    ArrayList<int[]> bLine = BresenhamLine(i, j, this.posX, this.posY);

                    if(this.scaryLights){
                        for(int[] item : bLine){
                        if(this.board.level.map[item[0]][item[1]].character.equals("#"))
                            this.visible[i][j] = false;
                        }
                    }
                    else {
                        // This gives us the line, but not in the order we need.
                        Collections.sort(bLine, new LocationComparator(this.posX, this.posY));

                        for(int k = 0; k < bLine.size(); k++){
                            if(!occluded && this.board.level.map[bLine.get(k)[0]][bLine.get(k)[1]].character.equals("#")){
                                // This block occludes vision
                                occluded = true;
                            }else if(occluded){
                                if(Math.pow(bLine.get(k)[0] - this.posX, 2) + Math.pow(bLine.get(k)[1] - this.posY, 2) > vision)
                                    this.visible[bLine.get(k)[0]][bLine.get(k)[1]] = false;
                            }
                        }
                    }
                }
            }
        }

        this.processBuffs();
    }

    private void processBuffs(){
        for(Status s : this.statusList){
            s.tick();
        }
        //for(int i = 0; )
        // TODO: remove nulled conditions
    }

    private boolean checkBuff(Status.condition check){
        for(Status s : this.statusList){
            if(s.condi == check && s.enabled){
                return true;
            }
        }
        return false;
    }

    private void attack(Entity target){
        // do some fancy shit here later
        target.applyDamage(10);
        this.board.statusAddHead("You slap the " + target.name + " around with a large trout!");
    }

}






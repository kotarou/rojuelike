import java.util.Random;
import java.awt.Color;
/**
 * Radiation is the element that is characterised by:
 * Small damage, long duration, long range and status effects
 */
public class Radiation extends Element
{
    private Random rand;

    public Radiation()
    {
        this.name = "Radiation";

        this.chargeMin = 1;
        this.chargeMax = 4;
        this.rand = new Random();
        this.charges = rand.nextInt((this.chargeMax - this.chargeMin) + 1) + this.chargeMin;

        this.prefixes   = new String[]{"Radiating", "Radiant", "Resplendent", "Reflecting", "Revealing", "Really bright", "Reactive"};
        this.roots      = new String[]{"Radiator", "Elephant's foot", "Sun", "Reactor"};
        this.suffixes   = new String[]{"Reflection", "Radiation", "Radiance", "Revelation", "Redundancy", "the Sun", "Chernobyl"};

        this.range      = rand.nextInt((25 - 0) + 1) + 0; // 0 is personal, n = dist
        this.m_range    = 2;
        this.targetType = 2; // 0 = buff, 1 = line, 2 = PBAoE
        this.damageType = 1;// 0 = normal, 1 = penetrating
        this.statusAff  = rand.nextInt((3 - 1) + 1) + 1; // 0 = none, 1 = haste, 2 = slow, 3 = poison
        this.duration   = rand.nextInt((100 - 0) + 1) + 0; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)
        this.m_duration = 4;
        this.damage     = 10;
        this.m_damage   = 0.5;
        this.self_damage = 0;
        this.color = Color.GREEN;
    }

    // Prefix
    public double prefix_mod(){
        return 1.5;
    }

    // Effect
    public double effect_mod(){
        return 1.5;
    }

    // Suffix
    public double suffix_mod(){
        return 1.5;
    }

}







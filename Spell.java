import java.util.Random;
import java.awt.Color;
import java.util.ArrayList;
/**
 * A single spell, of multiple elements
 */
public class Spell
{
    enum status {
        NORMAL, HASTE, SLOW, POISON, FEEBLE, XRAY
    }


    // Components of the spell's name
    private String prefix;
    private String root;
    private String suffix;
    public Color color;
    private Random rand;

    // Number of times it can be cast
    public int charges;

    //Targeting, range, etc
    public int range; // 0 is personal, n = dist
    public int targetType; // 0 = buff, 1 = line, 2 = PBAoE
    public int damageType; // 0 = normal, 1 = penetrating
    public int statusAff; // 0 = none, 1 = haste, 2 = slow, 3 = poison
    public int duration; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)

    public int damage;
    public int self_damage;

    public int random(int min, int max){
        return this.rand.nextInt((max - min) + 1) + min;
    }

    public int randi(int max){
        return this.random(0, max);
    }

    public Spell(Element prefix, Element effect, Element suffix, int charges)
    {
        this.rand = new Random();

        this.prefix = prefix.descriptiveName(0);
        this.root   = effect.descriptiveName(1);
        this.suffix = suffix.descriptiveName(2);

        this.charges = charges;

        // Roll for random effects
        this.range      = this.randi(10);
        this.targetType = this.range > 4 ? this.randi(1) : this.randi(2); // No PBAoE on the big ranges
        this.damageType = this.randi(1);
        this.statusAff  = this.randi(3);
        this.duration   = this.randi(10);

        // Spell component overrides
        this.range      = (int)(this.range * prefix.m_range * suffix.m_range) + effect.range;
        this.targetType = this.randi(10) <= 7 ? effect.targetType : this.targetType;
        this.damageType = this.randi(10) <= 7 ? effect.damageType : this.damageType;
        this.statusAff  = this.randi(10) <= 9 ? effect.statusAff : this.statusAff;
        this.duration   = (int)(this.duration * prefix.m_duration * suffix.m_duration) + effect.duration;

        // Data purely from spells
        this.damage = (int)(effect.damage * prefix.m_damage * suffix.m_damage);
        this.self_damage = (int)(effect.self_damage + prefix.self_damage + suffix.self_damage);

        ArrayList<Color> coloropt = new ArrayList<Color>();
        coloropt.add(prefix.color);
        coloropt.add(effect.color);
        coloropt.add(suffix.color);
        this.color = coloropt.get(this.randi(2));
        
        System.out.println(this.range + " " +
            this.targetType + " " +
            this.damageType + " " +
            this.statusAff  + " " +
            this.duration + " " +
            this.damage + " " +
            this.self_damage + " ");
    }

    public Spell(Element prefix, Element effect, Element suffix)
    {
        this(prefix, effect, suffix, prefix.charges + effect.charges + suffix.charges);
    }


    public String toSmallHTMLString(){
        String base =  "<span style='color: white;'>" + this.prefix.substring(0,1) + "" + this.root.substring(0,1) + "" + this.suffix.substring(0,1) + "</span>";
        String ch = "<span style='color: white;'> ("+this.charges+")</span>";
        return base + ch;
    }

    public String toHTMLString(){
        String base = "<span style='color: green;'>" + this.prefix.substring(0,1) + "<span style='color: white;'>" + this.prefix.substring(1,this.prefix.length()) + " " + this.root + " of " + this.suffix + "</span>";
        String s = this.charges > 1 ? "s" : "";
        String ch = "<span style='color: white;'> ("+this.charges+" charge" + s + " remaining)</span>";
        return base + ch;
    }

    public boolean impact(Entity target, Board board){
        if(target.destructable){
            target.applyDamage(this.damage);
            board.statusAddHead("Spell did " + this.damage + " damage to: " + target.name);
        }

        if(this.damageType == 1)
            return true;
        else
            return !target.destructable;
    }
}

import java.awt.Color;
/**
 * Entity is the interface for objects that appear within the game map
 *
 */
public class SpellEffect extends Entity
{

    public SpellEffect(String character, Color color, String htmlColor){
        this.character = character;
        this.color = color;
        this.htmlColor = htmlColor;
        this.name = "spelleffect";
        this.movable     = false;
        this.pushable     = false;
        this.destructable = false;
        this.pathable     = true;
        this.intelligent  = false;
        this.invisible = false;
    }
    
    public void act(){}

    public boolean applyDamage(int damage){return false;};

}

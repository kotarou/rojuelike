import java.awt.Color;

public class StairDown extends Entity
{
    /**
     * Constructor for objects of class Wall
     */
    public StairDown()
    {
        this.character = ">";
        this.color = Color.WHITE;
        this.htmlColor = "#ffffff";

        this.name = "stairDown";
        // Disallow movement across the wall
        this.pathable       = true;
        this.movable        = false;
        this.intelligent    = false;
        this.pushable       = false;
        this.destructable   = false;
    }

    // bah
    public void act(){}
}


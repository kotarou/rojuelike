public class Status {

    enum condition{
        NORMAL, HASTE, SLOW, POISON, FEEBLE, XRAY, CURSED
    }

    public condition condi;
    public int duration;
    public int elapsed;
    public boolean enabled;

    public Status(condition condi, int duration){
        this.condi = condi;
        // Duration -1 means infinite
        this.duration = duration;
        this.enabled = true;
    }

    public void tick(){
        if(!this.enabled)
            return;

        if(this.duration > 0)
            this.duration--;

        if(duration == 0)
            this.enabled = false;
    }

    public String toString(){
        String out = "";
        switch(this.condi){
            case NORMAL:
                out = out + " NORMAL ";
                break;
            case HASTE:
                out = out + " HASTE ";
                break;
            case SLOW:
                out = out + " SLOW ";
                break;
            case POISON:
                out = out + " POISON ";
                break;
            case FEEBLE:
                out = out + " FEEBLE ";
                break;
            case XRAY:
                out = out + " XRAY ";
                break;
            case CURSED:
                out = out + " CURSED ";
                break;
        }
        out = out + (this.duration == -1 ? "( inf )" : "(" + this.duration + ")" );
        return out;
    }

}

import java.util.Random;
import java.awt.Color;
/**
 * Stitch is the element characterised by:
 * healing special effect, medium damage, medium range
 */
public class Stitch extends Element
{
    private Random rand;

    public Stitch()
    {
        this.name = "Stitch";

        this.chargeMin = 1;
        this.chargeMax = 4;
        this.rand = new Random();
        this.charges = rand.nextInt((this.chargeMax - this.chargeMin) + 1) + this.chargeMin;

        this.prefixes   = new String[]{"Stitching", "Severing", "Sewing", "Suturing", "Servicable"};
        this.roots      = new String[]{"Needle", "Seam", "Thread"};
        this.suffixes   = new String[]{"Stitching", "the Ewe", "Knitting", "Seperation", "Security"};

        this.range      = rand.nextInt((4 - 1) + 1) + 1; // 0 is personal, n = dist
        this.m_range    = 0.8;
        this.targetType = rand.nextInt((1 - 0) + 1) + 0; // 0 = buff, 1 = line, 2 = PBAoE
        this.damageType = 0; // 0 = normal, 1 = penetrating
        this.statusAff  = rand.nextInt((2 - 0) + 1) == 1 ? 0 : 3; // 0 = none, 1 = haste, 2 = slow, 3 = poison
        this.duration   = rand.nextInt((7 - 1) + 1) + 1; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)
        this.m_duration = 1;
        this.damage     = rand.nextInt((20 - 10) + 1) + 10;
        this.m_damage   = 1; //rand.next((0.8 - 0.4) + 1.0) + 0.4;
        this.self_damage = -rand.nextInt((40 -10) + 5) + 0;
        this.color = Color.RED;
    }

    // Prefix
    public double prefix_mod(){
        return 1.5;
    }

    // Effect
    public double effect_mod(){
        return 1.5;
    }

    // Suffix
    public double suffix_mod(){
        return 1.5;
    }

}


import java.util.Random;
import java.awt.Color;
/**
 * Time is the elemnt characterised by
 * low range, long duration, low damage and status effects
 */
public class Time extends Element
{
    private Random rand;

    public Time()
    {

        this.name = "Time";

        this.chargeMin = 1;
        this.chargeMax = 4;
        this.rand = new Random();
        this.charges = rand.nextInt((this.chargeMax - this.chargeMin) + 1) + this.chargeMin;

        this.prefixes   = new String[]{"Ticking", "Tocking", "Tolling", "Temporal"};
        this.roots      = new String[]{"Timepiece", "Watch", "Clock", "Chime"};
        this.suffixes   = new String[]{"Entropy", "Decay", "the Ages", "Seasons Past", "of Things to Be", "of Memories Forgotten"};

        this.range      = rand.nextInt((3 - 0) + 1) + 0; // 0 is personal, n = dist
        this.m_range    = 0.3;
        this.targetType = rand.nextInt((1 - 0) + 1) + 0; // 0 = buff, 1 = line, 2 = PBAoE
        this.damageType = 0; // 0 = normal, 1 = penetrating
        this.statusAff  = rand.nextInt((2 - 0) + 1) + 0; // 0 = none, 1 = haste, 2 = slow, 3 = poison
        this.duration   = rand.nextInt((100 - 0) + 1) + 0; // 0 = doesn't stop, n = # rounds (a damage instance is over 1 round)
        this.m_duration = 4;
        this.damage     = 10;
        this.m_damage   = 0.3;
        this.self_damage = 0;
        this.color = Color.BLUE;
    }

    // Prefix
    public double prefix_mod(){
        return 1.5;
    }

    // Effect
    public double effect_mod(){
        return 1.5;
    }

    // Suffix
    public double suffix_mod(){
        return 1.5;
    }

}


import java.awt.Color;

public class Wall extends Entity
{
    /**
     * Constructor for objects of class Wall
     */
    public Wall()
    {
        this.character = "#";
        this.color = Color.WHITE;
        this.htmlColor = "#ffffff";

        this.name = "wall";
        // Disallow movement across the wall
        this.pathable       = false;
        this.movable        = false;
        this.intelligent    = false;
        this.pushable       = false;
        this.destructable   = false;
    }

    // bah
    public void act(){}
}

import java.awt.Color;

public class Water extends Entity
{
    /**
     * Constructor for objects of class Water
     */
    public Water()
    {
        this.character = "~";
        this.color = Color.BLUE;
        this.htmlColor = "#0000ff";

        this.pathable       = true;
        this.movable        = false;
        this.intelligent    = false;
        this.pushable       = false;
        this.destructable   = false;
    }

    // bah
    public void act(){}
}
